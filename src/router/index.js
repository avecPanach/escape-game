import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'Home',
        component: () =>
            import ('../views/Home.vue')
    },
    {
        path: '/about',
        name: 'About',
        component: () =>
            import ('../views/About.vue')
    },
    {
        path: '/emergency1',
        name: 'Emergency',
        component: () =>
            import ('../views/Desk1/Emergency.vue')
    },
    {
        path: '/documentation1',
        name: 'Documentation',
        component: () =>
            import ('../views/Desk1/Documentation.vue')
    },
    {
        path: '/doors1',
        name: 'Doors',
        component: () =>
            import ('../views/Desk1/Doors.vue')
    },
    {
        path: '/communication1',
        name: 'Communication',
        component: () =>
            import ('../views/Desk1/Communication.vue')
    },
    {
        path: '/alarm1',
        name: 'Alarm',
        component: () =>
            import ('../views/Desk1/Alarm.vue')
    },
    {
        path: '/fan1',
        name: 'Fan',
        component: () =>
            import ('../views/Desk1/Fan.vue')
    },
    {
        path: '/containment1',
        name: 'Containment',
        component: () =>
            import ('../views/Desk1/Containment.vue')
    },
    {
        path: '/help1',
        name: 'Help',
        component: () =>
            import ('../views/Desk1/Help.vue')
    },

    {
        path: '/emergency2',
        name: 'Emergency2',
        component: () =>
            import ('../views/Desk2/Emergency.vue')
    },
    {
        path: '/documentation2',
        name: 'Documentation2',
        component: () =>
            import ('../views/Desk2/Documentation.vue')
    },
    {
        path: '/doors2',
        name: 'Doors2',
        component: () =>
            import ('../views/Desk2/Doors.vue')
    },
    {
        path: '/communication2',
        name: 'Communication2',
        component: () =>
            import ('../views/Desk2/Communication.vue')
    },
    {
        path: '/alarm2',
        name: 'Alarm2',
        component: () =>
            import ('../views/Desk2/Alarm.vue')
    },
    {
        path: '/fan2',
        name: 'Fan2',
        component: () =>
            import ('../views/Desk2/Fan.vue')
    },
    {
        path: '/containment2',
        name: 'Containment2',
        component: () =>
            import ('../views/Desk2/Containment.vue')
    },
    {
        path: '/help2',
        name: 'Help2',
        component: () =>
            import ('../views/Desk2/Help.vue')
    },

    {
        path: '/emergency3',
        name: 'Emergency3',
        component: () =>
            import ('../views/Desk3/Emergency.vue')
    },
    {
        path: '/documentation3',
        name: 'Documentation3',
        component: () =>
            import ('../views/Desk3/Documentation.vue')
    },
    {
        path: '/doors3',
        name: 'Doors3',
        component: () =>
            import ('../views/Desk3/Doors.vue')
    },
    {
        path: '/communication3',
        name: 'Communication3',
        component: () =>
            import ('../views/Desk3/Communication.vue')
    },
    {
        path: '/alarm3',
        name: 'Alarm3',
        component: () =>
            import ('../views/Desk3/Alarm.vue')
    },
    {
        path: '/fan3',
        name: 'Fan3',
        component: () =>
            import ('../views/Desk3/Fan.vue')
    },
    {
        path: '/containment3',
        name: 'Containment3',
        component: () =>
            import ('../views/Desk3/Containment.vue')
    },
    {
        path: '/help3',
        name: 'Help3',
        component: () =>
            import ('../views/Desk3/Help.vue')
    }

]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router